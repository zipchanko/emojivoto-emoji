.PHONY: emoji-svc dependency

emoji-svc:
	$(MAKE) -C emojivoto-emoji-svc

dependency:
	go mod download
	go get -v -d ./...
