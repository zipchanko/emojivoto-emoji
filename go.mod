module gitlab.com/zipchanko/emojivoto-emoji

go 1.13

require (
	contrib.go.opencensus.io/exporter/ocagent v0.7.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/prometheus/client_golang v1.17.0
	go.opencensus.io v0.24.0
	google.golang.org/grpc v1.59.0
	google.golang.org/protobuf v1.31.0
)
